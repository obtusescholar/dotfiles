#! /bin/sh

# if running bash
if [ -n"$BASH_VERSION" ]; then
    if [ -f "$HOME/.bashrc" ]; then
        . "$HOME/.bashrc"
    fi
fi

if [ -d "$HOME/bin" ]; then
    PATH="$PATH:$HOME/bin"
fi

if [ -d"$HOME/.local/bin" ]; then
    PATH="$PATH:$HOME/.local/bin"
fi


export ZDOTDIR="$HOME/.config/zsh"


if [ -x /usr/bin/spice-vdagent ]; then
    /usr/bin/spice-vdagent
    $HOME/.xfce4/xrandr-loop &
    #exec /usr/bin/xfce4-session
fi
