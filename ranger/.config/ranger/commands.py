from shutil import which

from ranger.api.commands import Command


class fm(Command):
    """:fm <filemanager>

    Open current folder in file manager
    """

    __fileManagers = [
        "pcmanfm-qt",
        "thunar",
        "pcmanfm"]

    def __getFileManager(self):
        for fm in self.__fileManagers:
            if which(fm) is not None:
                yield fm

    def execute(self):
        if not self.arg(1):
            fm = next(self.__getFileManager())
        else:
            fm = self.arg(1)

        folder = self.fm.thisdir.path
        self.fm.run('(nohup ' + fm + ' ' + folder + ' &) &> /dev/null')

    def tab(self, tabnum):
        return ('fm ' + f for f in self.__getFileManager())
