set guicursor=n-v-c:block-Cursor
set autoindent
set encoding=utf-8
set expandtab
set fileformat=unix
set shiftround
set shiftwidth=4
set softtabstop=4
set tabstop=4
set cursorline


" Fix cursor with zsh
autocmd VimEnter * silent exec "! echo -ne '\e[1 q'"
autocmd VimLeave * silent exec "! echo -ne '\e[5 q'"


" Coloring
colo ron
syntax on
filetype plugin on
filetype indent on
filetype plugin on
filetype plugin indent on
if &term == "screen"
  set t_Co=256
endif


" Search
set ignorecase
set smartcase
set incsearch

" Key rebind
"" Sorting
vnoremap <Leader>s :sort<CR>

"" Unset the last search pattern
nnoremap <CR> :noh<CR><CR>

"" Leader
map , <Leader>

"" Moving between screens
map <C-H> <C-W>h
map <C-J> <C-W>j
map <C-K> <C-W>k
map <C-L> <C-W>l

"" Moving with wrapped lines
noremap j gj
noremap k gk

"" Copy till end of line
noremap Y y$

"" Indentation in visual returns into visual
vnoremap < <gv
vnoremap > >gv


" Functions
"" Relative number toggle
set relativenumber
set number
noremap <F3> :call RelNumToggle()<CR>
function! RelNumToggle()
    if(&relativenumber == 1)
        set nonu
        set nornu
    elseif(&number == 1)
        set relativenumber
    else
        set number
        set relativenumber
    endif
endfunc

"" Static number toggle
noremap <F4> :call StaNumToggle()<CR>
function! StaNumToggle()
    if(&relativenumber == 1)
        set nornu
        set number
    elseif(&number == 1)
        set nonu
    else
        set number
    endif
endfunc


" Toggle colorclumn
noremap <F2> :call CCToggle()<CR>
function! CCToggle()
    if(&colorcolumn == 0)
        set colorcolumn=81
        set textwidth=80
    elseif(&colorcolumn == 81)
        set colorcolumn=73
        set textwidth=72
    else
        set colorcolumn=0
        set textwidth=0
    endif
endfunc


" Plugins
"" Settings

""" NERDTree
noremap <leader>n :NERDTreeToggle<CR>
noremap <leader>f :NERDTreeFind<CR>
" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
" Close the tab if NERDTree is the only window remaining in it.
autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif


""" Airline
let g:airline_powerline_fonts = 1
let g:airline_theme="wombat"

""" CtrlP
"let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files . -co - exclude-standard', 'find %s -type f']
"let g:ctrlp_extensions = ['buffertag']
"noremap <leader>t :CtrlPBufTag<CR>

""" Taglist
"noremap <leader>t :TlistToggle<CR>

""" Tagbar
nmap <leader>t :TagbarToggle<CR>

"" Management
call plug#begin('~/.vim/plugged')
""" Syntax
Plug 'pangloss/vim-javascript'
Plug 'MaxMEllon/vim-jsx-pretty'
Plug 'uiiaoo/java-syntax.vim'
"Plug 'plasticboy/vim-markdown'

""" SidePanel
Plug 'scrooloose/nerdtree'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'ryanoasis/vim-devicons'
"Plug 'vim-scripts/taglist.vim'
Plug 'preservim/tagbar'

""" Panel
"Plug 'ctrlpvim/ctrlp.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

""" Functionality
Plug 'tpope/vim-surround'
call plug#end()
