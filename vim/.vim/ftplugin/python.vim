set softtabstop=4
set tabstop=4
set colorcolumn=80
set textwidth=79
let python_hilight_all=1


"" Toggle colorcolumn
noremap <F2> :call CCToggle()<CR>
function! CCToggle()
    if(&colorcolumn == 0)
        set colorcolumn=80
        set textwidth=79
    elseif(&colorcolumn == 80)
        set colorcolumn=73
        set textwidth=72
    else
        set colorcolumn=0
        set textwidth=0
    endif
endfunc
